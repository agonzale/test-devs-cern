# DEMO 2, Protect application with SSO

Add SSO integration

* Delete the existing route

```
Applications > Routes > tesforge (name of the route) > Actions > Delete
```

* Instantiate the template, setting an egroup, and the service 'test-app'.

```
Add to Project > Browse Catalog > Uncategorized > cern-sso-proxy (select)

AUTHORIZED_GROUPS: it-dep (or any other)
SERVICE_NAME: test-app
```

* Page is now protected by SSO and shown only to members of egroup.

* Need to change the code to display the user

* Set a web hook

```
Builds > builds > test-devs-cern > Configuration > Generic Webhook URL (copy)
```

Go to GitLab:

```
Settings > Integrations

URL: Previously copied URL

Add webhook
```

* Create merge request

Between "sso_proxy_support" and master

* Merge it

* Edit the config map to add more egroups

Resources > Config Maps > cern-sso-proxy

```
  <RequireALL>
    Require valid-user
    <RequireANY>
        Require shib-attr ADFS_GROUP it-dep
        Require shib-attr ADFS_GROUP atlas-readaccess-svn
    </RequireANY>
  </RequireALL>
```

* Done
